# App Engine using Flask


Before running or deploying this application, install the dependencies using
[pip](http://pip.readthedocs.io/en/stable/):

    pip install -t lib -r requirements.txt

Run tests
    pytest
	
Run locally
	export FLASK_APP=main.py
	export FLASK_DEBUG=1
	flask run

Deploy
	gloud app deploy
