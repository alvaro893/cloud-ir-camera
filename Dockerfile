# Don't use it on production
# image
FROM debian:latest
MAINTAINER Alvaro

# install software
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN apt-get clean

# copy files from host to container and set working directory
COPY . /app
WORKDIR /app

# install python dependencies locally
RUN pip install -r requirements.txt

# set enviroment variables
ENV FLASK_APP main.py
ENV FLASK_DEBUG 1

# set entry point
CMD ["./startApp.sh"]
ENTRYPOINT ["python"]
CMD ["-m", "flask", "run", "--host", "0.0.0.0"]

