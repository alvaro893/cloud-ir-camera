import json
import logging
import time
from flask import *
# from pymongo import MongoClient

app = Flask(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)6d %(threadName)s %(message)s')
app = Flask(__name__)
password = 'development_server'


# client = MongoClient('mongodb://admin:superadmin@ds159497.mlab.com:59497/ir-cloud-data')
# db = client['ir-cloud-data']
# get parameters only at the begining
# def get_parameters():
#     cursor = db.parameters.find()
#     dict = {}
#     for i in cursor:
#         dict[i['name']] = i['value']
#     cursor.close()
#     return dict
class Buffer:
    def __init__(self):
        self.buff = None
    def clear(self):
        self.buff = None
    def set(self, data):
        self.buff = data

    def empty(self):
        return self.buff == None

    def get(self):
        return self.buff


parameters = {}
cbuffer = Buffer()


@app.route('/', methods=['GET'])
def root():
    return "working"

@app.route('/', methods=['DELETE'])
def delete_all():
    parameters.clear()



@app.route('/parameters', methods=['PUT'])
def set_params():
    global parameters
    for k, v in request.form.iteritems():
        parameters[k] = v
    return 'ok'


@app.route('/people', methods=['POST'])
def submit_n_people():
    # request
    key = request.args['key']
    n_people = int(request.form['n_people'])
    assert key == password
    process(n_people)

    # response
    return jsonify(**parameters)


@app.route('/video', methods=['POST'])
def submit_frame():
    data = request.data.encode('base64')
    logging.debug("data:%s", data)
    # db.data.insert_one({'time': time.time(), 'data': data})
    return jsonify(**parameters)


@app.route('/video/buffer', methods=['POST'])
def submit_buff():
    """ to post a video buffer to the cloud. parameters are received in the response (camera side)"""
    data = request.data
    logging.debug("data length:%s", len(data))
    cbuffer.set(data)

    # response
    res = jsonify(**parameters)
    parameters.clear()
    return res

@app.route('/video/buffer', methods=['GET'])
def obtain_buff():
    """ to get the next buffer in the cloud. camera parameter must be add as query parameters (client side)"""
    global parameters
    logging.info("parameters received: " + str(request.args))

    # create dict from args
    for k, v in request.args.items():
        parameters[k] = v

    if cbuffer.empty():
        logging.debug('buffer is empty, sending 0 ...')
        return '0'
    else:
        return cbuffer.get()


@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request.')
    return 'An internal error occurred.', 500


def process(n_people):
    pass
    # db.people.insert_one({'time': time.time(), 'n_people': n_people})

#
#
# def update_parameter(name, value):
#     db.parameters.update_one({"name": name},
#                          {"$set": {"value": value}},
#                          upsert=True)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
